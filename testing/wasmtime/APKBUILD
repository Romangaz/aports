# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=wasmtime
pkgver=8.0.1
pkgrel=0
pkgdesc="Fast and secure runtime for WebAssembly"
url="https://wasmtime.dev/"
# others unsupported
arch="aarch64 x86_64"
license="Apache-2.0"
depends_dev="libwasmtime=$pkgver-r$pkgrel"
makedepends="cargo rust-wasm zstd-dev"
subpackages="libwasmtime $pkgname-dev"
source="https://github.com/bytecodealliance/wasmtime/releases/download/v$pkgver/wasmtime-v$pkgver-src.tar.gz
	system-zstd.patch
	"
builddir="$srcdir/wasmtime-v$pkgver-src"
# net: fetch dependencies
# check: custom_limiter_detect_os_oom_failure fails with oom for some reason
options="!check net"


prepare() {
	default_prepare

	git init
	# can't patch deps with vendor dir
	rm -fv .cargo/config.toml
	rm -rf vendor

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo build --frozen --release
	cargo build --frozen --release --manifest-path crates/c-api/Cargo.toml
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/wasmtime -t "$pkgdir"/usr/bin/
	install -Dm644 target/release/libwasmtime.so -t "$pkgdir"/usr/lib/
	cp -a crates/c-api/include "$pkgdir"/usr/
}

libwasmtime() {
	amove usr/lib
}

sha512sums="
bfead926c77888bc9530a2ad0c41217e19c1e45aa0b9a6207a8307c707c8b1321e305f175b5466af36f65adc2b8ae48c0654e4dc6a88b8c0796ba4603f18e7cc  wasmtime-v8.0.1-src.tar.gz
931620c9109cf9a5554191dcf322c5b3ee0c1934c06ed26812a64ed7cd041b019086f7608ccd3c48acdd21f316f72dc2f9c44ac825a6736e4a4e79abc3fcc225  system-zstd.patch
"
