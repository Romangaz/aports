# Maintainer: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
# Contributor: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
pkgname=kicad-library
pkgver=7.0.3
pkgrel=0
pkgdesc="Kicad component and footprint libraries"
url="https://kicad.github.io/"
# limited by kicad
arch="noarch !armv7 !armhf !riscv64 !s390x"
license="GPL-3.0-or-later"
makedepends="cmake samurai"
depends="kicad"
subpackages="$pkgname-3d:three_d"
source="
	https://gitlab.com/kicad/libraries/kicad-symbols/-/archive/$pkgver/kicad-symbols-$pkgver.tar.gz
	https://gitlab.com/kicad/libraries/kicad-footprints/-/archive/$pkgver/kicad-footprints-$pkgver.tar.gz
	https://gitlab.com/kicad/libraries/kicad-packages3D/-/archive/$pkgver/kicad-packages3D-$pkgver.tar.gz
	"
options="!check" # package only provides data files, so not tests possible

build() {
	cd "$srcdir"/kicad-symbols-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build

	cd "$srcdir"/kicad-footprints-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build

	cd "$srcdir"/kicad-packages3D-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install "$srcdir"/kicad-symbols-$pkgver/build
	DESTDIR="$pkgdir" cmake --install "$srcdir"/kicad-footprints-$pkgver/build
}

three_d() {
	DESTDIR="$subpkgdir" cmake --install "$srcdir"/kicad-packages3D-$pkgver/build

	# Remove .step version of 3D models; only .wrl versions are needed
	find "$subpkgdir" -name '*.step' -exec rm {} \;
}

sha512sums="
4e7bb28da4dbbb588f8c1c91ad23fc8a9eb78c4339e2fdbba0580e8b99e04edb5b4ce8c1eecc86c2e364607919dcd612cb9d608c54a49752d8c30851c298c5dc  kicad-symbols-7.0.3.tar.gz
de50a4de5b032f262526fd6d4538ec4760fc0ebdf1094e14538698cfb6acfc3098691c22ddde22d9ab9dab6de54b19b3703acb145643cd3d5ee016c9e0dad5bc  kicad-footprints-7.0.3.tar.gz
707cca9890d03d853bd14e3d753776780f99c227abbad1b943816021aec89bbf24169f1d0f11188028fb9523fea2c69e5569d6c11ab2ba282e4436e05464580e  kicad-packages3D-7.0.3.tar.gz
"
