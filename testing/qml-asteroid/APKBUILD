# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qml-asteroid
pkgver=0_git20230508
pkgrel=0
_commit="88c1de486f03a50e775bf8665d8c14ff00efd468"
pkgdesc="QML components, styles and demos for AsteroidOS"
url="https://github.com/AsteroidOS/qml-asteroid"
# armhf blocked by qt5-qtvirtualkeyboard
arch="all !armhf"
license="LGPL-2.1-only"
depends="qt5-qtvirtualkeyboard"
# grep is required for the asteroid-generate-desktop.sh script, which is used by other packages
depends_dev="
	grep
	mlite-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	mapplauncherd-qt-dev
	"
subpackages="$pkgname-dbg $pkgname-dev"
source="https://github.com/asteroidos/qml-asteroid/archive/$_commit/qml-asteroid-$_commit.tar.gz"
builddir="$srcdir/$pkgname-$_commit"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DINSTALL_QML_IMPORT_DIR=/usr/lib/qt5/qml
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev

	amove usr/share/*/cmake
	amove usr/bin
}

sha512sums="
5b73c6cf570fe9edcddf8e8e698e99d10631a94605fb35bb1f8358dd3b9debf081afc202b3693c1b3181c34bfe6c3631844211be1342ae88b0ba46909e185e00  qml-asteroid-88c1de486f03a50e775bf8665d8c14ff00efd468.tar.gz
"
