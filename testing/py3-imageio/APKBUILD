# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=py3-imageio
pkgver=2.28.1
pkgrel=0
pkgdesc="Python library that provides an easy interface to read and write a wide range of image data"
url="https://github.com/imageio/imageio"
license="BSD-2-Clause"
# ppc64le: test failures
# s390x: freeimage
arch="noarch !ppc64le !s390x"
depends="python3 py3-numpy py3-pillow freeimage"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-psutil py3-imageio-ffmpeg py3-fsspec"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/i/imageio/imageio-$pkgver.tar.gz"
builddir="$srcdir/imageio-$pkgver"
options="!check" # intentionally fail without internet(?), todo

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD"/build/lib IMAGEIO_NO_INTERNET=1 pytest -v
}

package() {
	python3 setup.py install --root="$pkgdir"

	# remove unneeded binaries
	# shellcheck disable=2115
	rm -r "$pkgdir"/usr/bin
}

sha512sums="
47c6de52d7393783167474fd9207b4827b74c0f0c6f80bb133e41df3f9c3e773784395e07214af2d8c590f7f517428f903dc2a3e4958892876cd7ed40bf3f7eb  py3-imageio-2.28.1.tar.gz
"
