# Contributor: Paul Bredbury <brebs@sent.com>
# Maintainer: gay <gay@disroot.org>
pkgname=icewm
pkgver=3.3.4
pkgrel=0
pkgdesc="Window manager designed for speed, usability and consistency"
url="https://github.com/ice-wm/icewm"
arch="all"
options="!check" # No test suite
license="LGPL-2.0-only"
subpackages="$pkgname-doc $pkgname-lang"
makedepends="
	alsa-lib-dev
	cmake
	fribidi-dev
	glib-dev
	imlib2-dev
	libao-dev
	libintl
	librsvg-dev
	libsm-dev
	libsndfile-dev
	libxcomposite-dev
	libxdamage-dev
	libxft-dev
	libxinerama-dev
	libxpm-dev
	libxrandr-dev
	markdown
	perl
	samurai
	"
source="https://github.com/ice-wm/icewm/releases/download/$pkgver/icewm-$pkgver.tar.lz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCFGDIR=/etc/icewm \
		-DENABLE_NLS=OFF \
		-DCONFIG_IMLIB2=ON \
		-DCONFIG_LIBRSVG=ON \
		-DENABLE_LTO=ON \
		-DDOCDIR=/usr/share/doc/icewm
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d7a11ed89eba6706d41e6ddb7b2d0d1932b8bf0eb0c1979a9edd14408b6a1e31a8c58ec1e61afffc7864369976dd80174944afbefa18537c919d4a36a4e41e7c  icewm-3.3.4.tar.lz
"
