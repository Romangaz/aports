# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=klettres
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/klettres"
pkgdesc="Learn the Alphabet"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	knewstuff-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/klettres-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8c16cfab1c12ea73981c28cba34d5ff4ed62e58bc722780b272e18f5e5a9509ad9920a1a09e37aa9e09f770da505d318a720200d5ceb751c3e0b904d39de108d  klettres-23.04.1.tar.xz
"
