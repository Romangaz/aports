# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kiconthemes
pkgver=5.106.0
pkgrel=0
pkgdesc="Support for icon themes"
# armhf blocked by extra-cmake-module
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	karchive-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kitemviews-dev
	kwidgetsaddons-dev
	qt5-qtsvg-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kiconthemes-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kiconloader_unittest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(kiconloader_unittest|kiconloader_resourcethemetest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
efcb0983af25d0fe0488f69ab2152a79ece0fd5f4643381770a95b4297b159a4606deab4c8ead7dd8a5f9f5805b5f116853e24f1d7295dd7eca9e292fe1a150d  kiconthemes-5.106.0.tar.xz
"
