# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=hcloud
pkgver=1.33.2
pkgrel=0
pkgdesc="Command-line interface for Hetzner Cloud"
url="https://github.com/hetznercloud/cli"
license="MIT"
arch="all"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/hetznercloud/cli/archive/v$pkgver/hcloud-$pkgver.tar.gz"
builddir="$srcdir/cli-$pkgver"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags "-X github.com/hetznercloud/cli/internal/version.Version=$pkgver" \
		-v \
		./cmd/hcloud

	./hcloud completion bash > $pkgname.bash
	./hcloud completion fish > $pkgname.fish
	./hcloud completion zsh > $pkgname.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 hcloud -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
2a0c3ff75ff402c9580328bcef28b1bdb88021f55585129b7112e837d9fedca10042278656ea60e3ad54b603c60d91f4845a555356385bf3b5bca938b32af826  hcloud-1.33.2.tar.gz
"
