# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkdcraw
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org"
pkgdesc="RAW image file format support for KDE"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	libraw-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkdcraw-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
0bef7d9b3216ce01b7f0bef91df5c01a112c8595441c6d61a6dca11d9ac244ec77a2173d523951d54fc96abe5dd502a02888e195ac52901d79461b2fce3adfec  libkdcraw-23.04.1.tar.xz
"
