# Contributor: Luca Weiss <luca@z3ntu.xyz>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=audiotube
pkgver=23.04.1
pkgrel=0
pkgdesc="Client for YouTube Music"
url="https://invent.kde.org/plasma-mobile/audiotube"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later"
depends="
	gst-plugins-good
	kirigami-addons
	kirigami2
	py3-ytmusicapi
	qt5-qtimageformats
	qt5-qtmultimedia
	yt-dlp
	"
makedepends="
	extra-cmake-modules
	kcrash-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	py3-pybind11-dev
	python3-dev
	qt5-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/audiotube-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel	 \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
62e6b1649d6821acb89063fe37de47ceb31bfc9fe36178131fa288a2214f576736ed7bd2434326069a4f0db7579e01293882dd3464c492354f66175edec65e92  audiotube-23.04.1.tar.xz
"
