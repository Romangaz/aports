# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kjs
pkgver=5.106.0
pkgrel=0
pkgdesc="Support for JS scripting in applications"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND BSD-3-Clause AND MIT"
depends_dev="qt5-qtbase-dev perl-dev pcre-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kjs-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
23f0d61c28208ce8a60ceb25881a86557595b447e609d52b2f886478162f79c0062e98961623f35fea0cd06d6e2d5aa77c09f3531208042cfcefa2aa2d629085  kjs-5.106.0.tar.xz
"
