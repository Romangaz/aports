# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=dolphin
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/system/dolphin"
pkgdesc="KDE File Manager"
license="GPL-2.0-only"
depends="
	udisks2
	kio-extras
	"
depends_dev="
	baloo-dev
	baloo-widgets-dev
	kactivities-dev
	kbookmarks-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kiconthemes-dev
	kinit-dev
	kio-dev
	knewstuff-dev
	knotifications-dev
	kparts-dev
	ktextwidgets-dev
	kuserfeedback-dev
	kwindowsystem-dev
	phonon-dev
	qt5-qtbase-dev
	solid-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	ruby-test-unit
	samurai
	"
checkdepends="
	xvfb-run
	dbus
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/dolphin-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kfileitemmodeltest, placesitemmodeltest, dolphinmainwindowtest and kitemlistcontrollertest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(kfileitemmodel|placesitemmodel|dolphinmainwindow|kitemlistcontroller)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
9211d171ed3e7cc293af0757eea113d01411dafbdd3d4bc53b2a21302b44973705c06a7420a23983d2ed026d1a7205f07eb982526c913f6892a2d7115197724e  dolphin-23.04.1.tar.xz
"
