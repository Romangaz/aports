# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=aws-c-s3
pkgver=0.3.0
pkgrel=0
pkgdesc="AWS C99 library implementation for communicating with the S3 service"
url="https://github.com/awslabs/aws-c-s3"
# s390x: aws-c-common
# arm*, ppc64le: aws-c-io
arch="all !armhf !armv7 !ppc64le !s390x"
license="Apache-2.0"
makedepends="
	aws-c-auth-dev
	aws-c-cal-dev
	aws-c-common-dev
	aws-c-compression-dev
	aws-c-http-dev
	aws-c-io-dev
	aws-c-sdkutils-dev
	aws-checksums-dev
	cmake
	curl-dev
	s2n-tls-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-c-s3/archive/refs/tags/v$pkgver.tar.gz"
options="!check"  # The unit tests require an AWS account with S3 buckets set up in a particular way.

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/aws-c-s3
}

sha512sums="
1298b9cd027633734ae91412d6fbb2256cf2b57e7c2cd4b5d16c96c975ef87ee803f3daee5f8dae3b0279e9433baac54018f992458a6a56866650c62c447684f  aws-c-s3-0.3.0.tar.gz
"
