# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ksmtp
pkgver=23.04.1
pkgrel=0
pkgdesc="Job-based library to send email through an SMTP server"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kio
arch="all !armhf !s390x !riscv64"
url="https://kontact.kde.org"
license="LGPL-2.1-or-later"
depends_dev="
	cyrus-sasl-dev
	kcoreaddons-dev
	ki18n-dev
	kio-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksmtp-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c90f1d4a09a0c2867f7b3b1ca23866399bab282e815d1b89ef41cc54b3cd271da6163f7ce07680433106b348e49896fa8e0b8c6c7a5afe962af7f9dad760d353  ksmtp-23.04.1.tar.xz
"
