# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=umbrello
pkgver=23.04.1
pkgrel=0
arch="all !armhf !s390x !riscv64" # Blocked by extra-cmake-modules and rust
url="https://umbrello.kde.org/"
pkgdesc="GUI for diagramming Unified Modelling Language (UML)"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdelibs4support-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	ktexteditor-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/umbrello-$pkgver.tar.xz"
options="!check" # Broken

case "$CARCH" in
	ppc64le|armv7) options="!check";; # FIXME: testsuite fails
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_KF5=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f7c8fde4b89ddb0d0732efd0fa7f7c080f3456cca55b892cf5d94d203175b43d94f97d2a8ea6d44d32737c9fa41021c38c4820305f7a2bb090d68f7b4ada1aca  umbrello-23.04.1.tar.xz
"
