# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=lagrange
pkgver=1.15.9
pkgrel=0
pkgdesc="Beautiful Gemini client"
url="https://gmi.skyjake.fi/lagrange"
license="BSD-2-Clause"
arch="all"
makedepends="
	cmake
	fribidi-dev
	harfbuzz-dev
	libunistring-dev
	libwebp-dev
	mpg123-dev
	openssl-dev
	pcre2-dev
	samurai
	sdl2-dev
	zip
	zlib-dev
	"
subpackages="$pkgname-dbg $pkgname-doc"
source="https://git.skyjake.fi/gemini/lagrange/releases/download/v$pkgver/lagrange-$pkgver.tar.gz"
options="!check" # no test suite

[ "$CARCH" = "riscv64" ] && options="$options textrels"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_POPUP_MENUS=OFF \
		-DENABLE_RESIZE_DRAW=OFF \
		-DENABLE_X11_XLIB=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
81f43676a1ea19310a2fd6541a9767147813a434a1156c51ac30def62d29d0ed48eb29cfef2dd70081913ba3db50bb29ef81ac54e7ee52d4ab8e0f1db4bee29e  lagrange-1.15.9.tar.gz
"
