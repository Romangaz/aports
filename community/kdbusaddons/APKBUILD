# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdbusaddons
pkgver=5.106.0
pkgrel=0
pkgdesc="Addons to QtDBus"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	shared-mime-info
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdbusaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Requires running dbus-daemon

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
67e7f8301e785eb5461045160ab2fedcda72f3923ea1d91ee8cd4a693d2a01ad35c04ef85cc134bc9cf6f115a5dc93369b18b50f82ae246aea8efc278a71f2dc  kdbusaddons-5.106.0.tar.xz
"
